# Definición de una clase
class ClasePueba():
    var1 = 4
    var2 = "Pruebas"
    var3 = ["Madrugada", "Mañana", "Tarde", "Noche"]
    var4 = False

    # Funciones
    def metodoPrueba(self):
        self.var4 = True
    # Métodos

    def mname(self, arg):
        pass


# Objeros
objetoPrueba = ClasePueba()
print(objetoPrueba.var1)
objetoPrueba.metodoPrueba()
print(objetoPrueba.var4)
