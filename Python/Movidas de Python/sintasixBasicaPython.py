# declaracion de variables básicas
var1 = "Hola bb"

var2 = 5

var3 = 5.7

var4 = True

print(var1)
print(var2)
print(var3)
print(type(var1))
print(type(var4))

# operadores básicos
print(5+4)
print(5-4)
print(5*4)
print(5/4)
print(9//2)  # Para que devuelve el cociente sin decimales

# Condicionales
palabra = "Hola Buens"
if palabra == "Hola Buenas":
    print(palabra)
else:
    print("No")

# Bucles
# Bucles For
for i in [1, 2, 3]:
    print("Hola")
    # En Python los bucles for funcionan de manera diferente que en java, el i
    # no hace de contador, recorre los valores.

for i in ["otoño", "primavera", "verano", "invierno"]:
    print(i)

for i in range(5):
    print(i)
    # Con la función range(num) no hace falta utilizar una lista para sacar el
    # número de vueltas

for i in "enflogil@gmail.com":
    print(i, end="")
    # Con un for recorren facilmente los caracteres de un string
    # Con end podemos especificar los espacio entre un bucle y otro en la conso

# Bucle While
cond = 2
while cond != 2:
    print("UwU")

# Listas

lista1 = ["Pepe", "Juan", "Iñigo Montoya", "Tu mama"]

print(lista1)
print(lista1[2])
print(lista1[-3])
print(lista1[0:2])  # Muestra los elementos desde el primero hasta un límite

'''
Las listas en Python tienen la particularidad de que si pones un valor negativo
empezará a contar desde atrás, empezando por el último y acabando por el
primero.
'''
lista1.append("Milhouse")
# Con este metodo podemos añadir un nuevo elemento a la lista
lista1.remove("Milhouse")
# Elimina un elemento de una lista
print(lista1)
lista1.extend(["Moby Dick", "Nobu", "La navidad"])
# Añade más elementos de una vez
print(lista1)
print(lista1.index("Moby Dick"))
# Indica la posicion del elemento dentro de la lista
print("Juliana" in lista1)
# Indica si un elemento se encuentra dentro de la lista
