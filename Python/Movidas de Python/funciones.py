# Sintaxis de una función
parametro1 = 2
parametro2 = 3.45


def nombreFuncion(para1, para2):
    suma = para1+para2
    return suma  # Opcional


print(nombreFuncion(parametro1, parametro2))
