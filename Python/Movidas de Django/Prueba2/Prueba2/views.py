from django.http import HttpResponse
import datetime
from django.template import Template, Context


def prueba_plantillas(request):
    doc_externo = open("C:/Users/enflo/OneDrive/Documentos/GS DAM/2º DAM/Prácticas/Python/Movidas de Django/Prueba2/Prueba2/Plantillas/plantilla1.html")

    plt = Template(doc_externo.read())

    doc_externo.close()

    ctx = Context()

    documento = plt.render(ctx)
    return HttpResponse(documento)


def fecha(request):
    fechaActual = datetime.datetime.now()

    return HttpResponse(fechaActual)


def calculaEdad(request, anio):

    edadActual = 19
    periodo = anio - 2021
    edadFutura = edadActual + periodo
    documento = "<html><body>En el año %s tendrás %s </body></html>" %(anio, edadFutura)
    return HttpResponse(documento)


def calculaEdad2(request, edadActual, anio):

    periodo = anio - 2021
    edadFutura = edadActual + periodo
    documento = "<html><body>En el año %s tendrás %s </body></html>" %(anio, edadFutura)
    return HttpResponse(documento)


def pasar_valores_a_la_plantilla(request):
    nombre = "Juan"
    doc_externo = open("C:/Users/enflo/OneDrive/Documentos/GS DAM/2º DAM/Prácticas/Python/Movidas de Django/Prueba2/Prueba2/Plantillas/plantilla2.html")
    plt = Template(doc_externo.read())

    doc_externo.close()
    # Para introducir un parametro a la plantilla la pasaremos por el Context()
    ctx = Context({"nombre_persona": nombre, "apellido_persona": "Díaz", "temas": ["Hola", "a", "todos", "soy", " yo"]})
    """
    Entre los parentesis del Context, ponemos entre llaves el termino con el
    que lo vamos a identificar (en este caso nombre_persona) y pasamos el
    valor que vamos a utilizar en la plantilla (en este caso nombre)
    """
    documento = plt.render(ctx)
    return HttpResponse(documento)
