"""Prueba2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from Prueba2.views import fecha, calculaEdad, calculaEdad2, prueba_plantillas, pasar_valores_a_la_plantilla

urlpatterns = [
    path('admin/', admin.site.urls),
    path('plantilla/', prueba_plantillas),
    path('fecha/', fecha),
    # Introducir parametros en la URL
    path("edad/<int:anio>", calculaEdad),
    # De esta forma especificaremos que el parametro introducido es de un tipo
    path("edades/<int:edadActual>/<int:anio>", calculaEdad2),
    # Dos URLs no se pueden llamar igual
    path("plantilla2/", pasar_valores_a_la_plantilla)
]
